const WebSocket = require('ws');
const server = new WebSocket.Server({port: 8080});

function incoming(message) {
	const b = Buffer.from(message);
	console.log('🚀 ~ BUFFER en string :', b.toString());

	console.log('indexJS message reçu : ', message);
	// this.send(`INDEXJS voici ....${message}`);
}

// server.on('connection', (socket) => {
// 	socket.on('message', incoming.bind(message));
// 	socket.send('bonjour, le serveur est connecté');
// });

server.on('connection', (socket) => {
	socket.on('message', (message) => {
		console.log('message BRUT : ', message);
		const b = Buffer.from(message);
		console.log('🚀 ~ BUFFER en string :', b.toString());
		socket.send(b.toString());
	});
});

server.on('listening', () => {
	console.log('Le serveur WebSocket écoute sur le port 8080');
});

server.on('error', (error) => {
	console.error('Erreur du serveur WebSocket:', error);
});

// server.close(() => {
// 	console.log('Le serveur WebSocket est fermé');
// });
