const ws = new WebSocket('ws://localhost:8080');

ws.onopen = function () {
	console.log('Connected to server');
	ws.send('envoi OK');
};

ws.onmessage = function (event) {
	console.log('Message from server: ', event.data);
};

ws.onclose = function () {
	console.log('Disconnected from server');
};

ws.onerror = function (error) {
	console.log('WebSocket error: ', error);
};
