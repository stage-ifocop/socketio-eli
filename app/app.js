const sendBtn = document.getElementById('sendBtn');
console.log('🚀 ~ sendBtn:', sendBtn, 'qui est un :', typeof sendBtn);
const sendBtn2 = $('#sendBtn');
console.log('🚀 ~ sendBtn2:', sendBtn2, 'qui est un :', typeof sendBtn2);

const messInput = document.getElementById('messInput');
messInput.focus();

sendBtn.addEventListener('click', (e) => {
	e.preventDefault();
	const messInputJQ = $('#messInput');

	console.log('🚀 ~ messInput:', messInputJQ);
	const messTxt = messInputJQ.val().trim();
	console.log('🚀 ~ messTxt:', messTxt);

	const allMess = document.querySelectorAll('#message');
	console.log('🚀 ~ allMess:', allMess.length);

	if (messTxt !== '') {
		const messEl = document.createElement('div');
		messEl.textContent = messTxt;
		messEl.classList.add('alert', 'alert-secondary', 'buble1');
		messEl.id = 'message';
		document.getElementById('allMess').appendChild(messEl);
		messInputJQ.val(''); // Utilisation correcte de jQuery pour réinitialiser la valeur
		messInput.focus();
		messEl.scrollIntoView();
	}
});
